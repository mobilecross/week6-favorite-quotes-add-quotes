import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,AlertController, ToastController  } from 'ionic-angular';
import { Quote } from '../../data/quote.interface';
import { QuotesService } from '../../services/quotes';
import { QuotePage } from '../quote/quote';
import { SettingsService } from '../../services/settings';

/**
 * Generated class for the FavoritesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage{

  quoteCollection:Quote[];
  constructor(private quotesService:QuotesService, public navCtrl: NavController, public navParams: NavParams,
  private modalCtrl:ModalController, private settingsSrv: SettingsService,
  private alertCtrl: AlertController, private toastCtrl : ToastController) {
  }

  ionViewWillEnter(){
   this.quoteCollection=this.quotesService.getFavoriteQuotes();
  }

  onAddQuote(quote:Quote){
    this.quotesService.addQuoteToFavorites(quote);
  }

  onShowToast(){
    let toast = this.toastCtrl.create({
      message : "New Quote was added",
      duration: 3000,
      position: "bottom"
    });
    toast.present();
  }
  
  onShowAddForm(){
    const alert = this.alertCtrl.create({
      title: "Add New Quote",
      //subTitle: "",
      //message: "Are you sure want to add quotes to favorite?",
      inputs: [
        {
          name: 'author',
          placeholder: 'Author'
        },
        {
          name: 'quotes',
          placeholder: 'Quotes'
        }
      ],
      buttons:[
        {
          text: "Ok",
          handler:data=>{
              //quote= {person:data.author, text:data.quotes };
              //console.log(quote.text);
              this.onAddQuote({id:'x',person:data.author, text:data.quotes});
              console.log(this.quotesService);
              this.quoteCollection= this.quotesService.getFavoriteQuotes();
              this.onShowToast();
             
          }
        },
        {
          text: "Cancel",
          role:'cancel',
          handler:()=>{
              console.log("No");
          }
        }
      ]
    });
    alert.present();
  }

  showQuote(quote:Quote){
    console.log(quote);
    let modal = this.modalCtrl.create(QuotePage,{quoteText:quote.text, quotePerson: quote.person});
    modal.present();
    modal.onDidDismiss(
      (remove:boolean)=>{
        if(remove){
          this.quotesService.removeQuoteFromFavorites(quote);
          console.log(this.quoteCollection);
          this.quoteCollection = this.quotesService.getFavoriteQuotes();
        }
      }
    );
  }

  removeQuote(quote:Quote){
    this.quotesService.removeQuoteFromFavorites(quote);
    this.quoteCollection= this.quotesService.getFavoriteQuotes();
  }

  setBgColor(){
    return this.settingsSrv.isAltBackground() ? 'altBg2' : 'altBg';
  }


}
