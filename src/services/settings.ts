export class SettingsService{
    private altBackground = false;

    setBackground(isAlt:boolean){
        this.altBackground = isAlt;
    }
    
    isAltBackground(){
        //console.log(this.altBackground);
        return this.altBackground;
    }
}